# Repo of images to be used for my thesis committee meting 2018

This contains some images to be used during my thesis committee meeting in 2018.

They should be informative about:

- Cx-BG-Th-Cx loop description & role in bhv control.
- Example tasks used to explore motor control & role of RWD expectation (1DR).
- Behavioral assay to be used & why it's relevant.
- Hypothesis and objectives.

* Behavioral results:
 - Recapitulation of monkey results.
 - Congruency effect over reaction and movement times.
 - Transitions slopes differences by transition type.
 - Counterfactual learning.

* Neural correlates of behavior:
 - Example rasters.
 - ROC motivation.
 - ROC results:
  - ROC space per event.
  - ROC space exploration wTDA
 - Neuro pop results:
  - PCA motivation
  - PCA results.
  - Possible TDA?
